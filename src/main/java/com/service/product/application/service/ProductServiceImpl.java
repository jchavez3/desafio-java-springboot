package com.service.product.application.service;

import com.service.product.application.dto.ProductDTO;
import com.service.product.application.request.CreateProductRequest;
import com.service.product.application.request.UpdateProductRequest;
import com.service.product.domain.business.ProductBusiness;
import com.service.product.domain.model.Product;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductBusiness productBusiness;

    private DozerBeanMapper mapper;

    @Autowired
    public ProductServiceImpl(ProductBusiness productBusiness, DozerBeanMapper mapper) {
        this.productBusiness = productBusiness;
        this.mapper = mapper;
    }

    @Override
    public void save(CreateProductRequest request) {
        productBusiness.save(mapper.map(request, Product.class));
    }

    @Override
    public void update(UpdateProductRequest request) {
        productBusiness.update(mapper.map(request, Product.class));
    }

    @Override
    public ProductDTO remove(String productId) {
        var removeProduct = productBusiness.getById(productId);

        if(removeProduct == null)
            return null;

        productBusiness.remove(productId);

        return mapper.map(removeProduct, ProductDTO.class);
    }

    @Override
    public List<ProductDTO> getAll() {
        var products = productBusiness.getAll();

        return products
                .stream()
                .map(product -> mapper.map(product, ProductDTO.class))
                .filter(Objects::nonNull)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public ProductDTO getById(String productId) {
        var product = productBusiness.getById(productId);
        return mapper.map(product, ProductDTO.class);
    }
}
