package com.service.product.application.service;

import com.service.product.application.dto.ProductDTO;
import com.service.product.application.request.CreateProductRequest;
import com.service.product.application.request.UpdateProductRequest;

import java.util.List;

public interface ProductService {
    void save(CreateProductRequest request);
    void update(UpdateProductRequest request);
    ProductDTO remove(String productId);
    List<ProductDTO> getAll();
    ProductDTO getById(String productId);
}
