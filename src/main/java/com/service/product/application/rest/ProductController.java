package com.service.product.application.rest;

import com.service.product.application.dto.ProductDTO;
import com.service.product.application.request.CreateProductRequest;
import com.service.product.application.request.UpdateProductRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

public interface ProductController {

    @PostMapping("/products")
    ResponseEntity<Void> add(@RequestBody CreateProductRequest request);

    @GetMapping("/products")
    ResponseEntity<List<ProductDTO>> getProducts();

    @GetMapping("/products/{productId}")
    ResponseEntity<ProductDTO> getById(@PathVariable String productId);

    @GetMapping("/products/search")
    ResponseEntity<List<ProductDTO>> getBySearch(@RequestParam Optional<String> q,
                                                 @RequestParam Optional<Double> min_price,
                                                 @RequestParam Optional<Double> max_price);

    @DeleteMapping("/products/{productId}")
    ResponseEntity<Void> remove(@PathVariable String productId);

    @PutMapping("products/{productId}")
    ResponseEntity<Void> update(@PathVariable String productId, @RequestBody UpdateProductRequest request);

}
