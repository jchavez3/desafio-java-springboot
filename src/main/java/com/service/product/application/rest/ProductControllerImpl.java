package com.service.product.application.rest;

import com.service.product.application.dto.ProductDTO;
import com.service.product.application.request.CreateProductRequest;
import com.service.product.application.request.UpdateProductRequest;
import com.service.product.application.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;


public class ProductControllerImpl implements ProductController{

    private ProductService productService;

    Logger log = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    public ProductControllerImpl(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public ResponseEntity<Void> add(CreateProductRequest request) {
        log.info("start postProduct");

        productService.save(request);

        log.info("finish postProduct");

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<ProductDTO>> getProducts() {
        log.info("start getProducts");

        var response = productService.getAll();

        log.info("finish getProducts");

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ProductDTO> getById(String productId) {
        log.info("start getById");

        var productDTO = productService.getById(productId);

        if(productDTO == null) {
            log.info("finish getById, product not found");

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        log.info("finish getById");

        return new ResponseEntity<>(productDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<ProductDTO>> getBySearch(Optional<String> q, Optional<Double> min_price, Optional<Double> max_price) {
        return null;
    }

    @Override
    public ResponseEntity<Void> remove(String productId) {
        log.info("start deleteProduct");

        var removedProduct = productService.remove(productId);

        if(removedProduct == null) {
            log.info("finish deleteProduct, product not found");

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        log.info("finish deleteProduct");

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> update(String productId, UpdateProductRequest request) {
        log.info("start updateProduct");

        var productDTO = productService.getById(productId);

        if(productDTO == null) {
            log.info("finish updateProduct, product not found");

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        productService.update(request);

        log.info("finished updateProduct");

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
