package com.service.product.application.rest.exception;

import lombok.*;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class ExceptionResponse {
    private Integer statusCode;
    private String message;
}
