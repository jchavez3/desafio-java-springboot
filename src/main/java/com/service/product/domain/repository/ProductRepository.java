package com.service.product.domain.repository;

import com.service.product.domain.model.Product;

import java.util.List;

public interface ProductRepository {
    void save(Product product);
    void remove(Product product);
    List<Product> getAll();
    Product getById(String productId);
}
