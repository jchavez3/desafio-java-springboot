package com.service.product.domain.business;

import com.service.product.domain.model.Product;

import java.util.List;

public interface ProductBusiness {
    void save(Product product);
    void update(Product product);
    void remove(String productId);
    List<Product> getAll();
    Product getById(String productId);
}
