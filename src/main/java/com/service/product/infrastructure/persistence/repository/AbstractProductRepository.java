package com.service.product.infrastructure.persistence.repository;

import javax.servlet.http.HttpServletRequest;

public abstract class AbstractProductRepository {

    HttpServletRequest httpServletRequest;

    public AbstractProductRepository(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
    }

}
