package com.service.product.infrastructure.persistence.repository;

import com.service.product.domain.model.Product;
import com.service.product.domain.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Component
public class ProductRepositoryImpl extends AbstractProductRepository implements ProductRepository {

    private MongoTemplate mongoTemplate;

    @Value("${spring.data.mongodb.collection}")
    private String defaultCollection;

    private HttpServletResponse httpServletResponse;

    @Autowired
    public ProductRepositoryImpl(HttpServletRequest httpServletRequest, MongoTemplate mongoTemplate, HttpServletResponse httpServletResponse) {
        super(httpServletRequest);

        this.mongoTemplate = mongoTemplate;
        this.httpServletResponse = httpServletResponse;
    }

    @Override
    public void save(Product product) {
        mongoTemplate.save(product, defaultCollection);
    }

    @Override
    public void remove(Product product) {
        mongoTemplate.remove(product, defaultCollection);
    }

    @Override
    public List<Product> getAll() {
        return mongoTemplate.findAll(Product.class, defaultCollection);
    }

    @Override
    public Product getById(String productId) {
        var criteria = Criteria.where("_id").is(productId);

        Query query = new Query();

        query.addCriteria(criteria);

        return mongoTemplate.findOne(query, Product.class, defaultCollection);
    }
}
